var init = function() {

  /*Q3*/
  addAllFilms();

  /*Q4*/
  let filter = document.getElementById('filter');
  filter.addEventListener('keyup', filterFilms);
  filter.value='';

  /*Q5*/
  let showDetails = document.getElementById('showDetails');
  showDetails.checked = true;
  showDetails.addEventListener('change', toggleDetails);

}

/*Chargement de la page*/
window.addEventListener('DOMContentLoaded', init);

/*Q3*/
var addAllFilms = function() {
  let filmContainer = document.getElementById('films');
  for(let i = 0; i < filmData.length; i++) {
    filmContainer.appendChild(createFilm( i ));
  }
}


var createFilm = function(index) {
  /*
      A compléter Q3
  */

  /*Q6*/
  divFilm.addEventListener('mouseover', displayText);

  /*Q7*/
  divFilm.addEventListener('mouseout', removeText);

  /*Q8*/
  divFilm.addEventListener('click', selectFilm);

  return divFilm;

}

var filterFilms = function() {
  /*Q4 A compléter*/
}

var toggleDetails = function() {
  /*
    Q5 A compléter
  */
}

var displayText = function() {
  /*
    Q6 A compléter
  */
}

var removeText = function() {
  /*
    Q7 A compléter
  */
}


var nextFreeSelectSlot = function() {
  let selectUn = document.querySelector('#select1 .film');
  if (selectUn == undefined) {
    return 1;
  }
  let selectDeux = document.querySelector('#select2 .film');
  if (selectDeux == undefined) {
      return 2;
  }
  return 0;
}

var selectFilm = function() {
  let nextFreeSlot = nextFreeSelectSlot();
  if (nextFreeSlot == 0) {
    window.alert('vous avez déjà sélectionné deux films');
  }
  else {
    let selectUn = document.getElementById('select'+nextFreeSlot);
    let currentSpan = selectUn.querySelector('span');
    selectUn.insertBefore(this, currentSpan);

    this.removeEventListener('click', selectFilm);

    /*Q8*/
    this.addEventListener('click', unselectFilm);

      let details = document.getElementById('details');
      details.textContent = '';
  }
}

/*Q8*/
var unselectFilm = function() {
  /*
    Q8 A compléter
  */
}
